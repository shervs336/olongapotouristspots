<?php
	$active = "location";
	
	require_once "../config.php";

	require_once "../layout/header.php";
	
	require_once "../layout/navs.php";
	
	require_once "../layout/location/location-info.php";
	
	require_once "../layout/footer.php";
?>