<?php

	$active = "about";
	
	require_once "config.php";
	
	require_once "layout/header.php";
	
	require_once "layout/navs.php";
	
	require_once "layout/about.php";

	require_once "layout/footer.php";

?>