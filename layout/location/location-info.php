
	<!--- List of Olongapo Tourists Spots --->
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page-header">Marikit Park</h1>
			</div>
			<div class="col-xs-12 col-sm-9">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<p class="rating">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
						</p>
						<p class="address">
							Hospital Rd, Olongapo, Zambales, Philippines
						</p>
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h2 class="panel-title">Opening Hours</h2>
							</div>
							<div class="panel-body">
								<p>Monday - Sunday: 6:00am - 9:00pm</p>
							</div>
						</div>
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h2 class="panel-title">Tourist Type</h2>
							</div>
							<div class="panel-body">
								<p>
									<span class="label label-warning">Family</span>
									<span class="label label-warning">Park</span>
									<span class="label label-warning">Playground</span>
								</p>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h2 class="panel-title">Location Map</h2>
							</div>
							<div class="panel-body">
								<img class="img-responsive img-rounded" src="http://maps.googleapis.com/maps/api/staticmap?center=14.83647,120.28327&amp;zoom=17&amp;size=400x350&amp;sensor=false&amp;markers=14.83647,120.28327&amp;scale=2" />
							</div>		
						</div>						
					</div>
					<div class="col-xs-12">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h2 class="panel-title">History</h2>
							</div>
							<div class="panel-body">
								<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
								<p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>
							</div>		
						</div>						
					</div>
					<div class="col-xs-12">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h2 class="panel-title">Comments and Reviews</h2>
							</div>
							<div class="panel-body review-container">
								<div class="row">
									<div class="review">
										<div class="well well-sm review-header">
											<span class="rating">
												<span class="glyphicon glyphicon-star"></span>
												<span class="glyphicon glyphicon-star"></span>
												<span class="glyphicon glyphicon-star"></span>
												<span class="glyphicon glyphicon-star-empty"></span>
												<span class="glyphicon glyphicon-star-empty"></span>
											</span>
											<span class="reviewAuthor">
												Sherwin Rhey Condez
											</span>
											<small class="reviewTimestamp">18 April 2016</small>
										</div>
										<div class="col-xs-12">
											<p>What a great place. I can;t say enough good things about it.</p>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="review">
										<div class="well well-sm review-header">
											<span class="rating">
												<span class="glyphicon glyphicon-star"></span>
												<span class="glyphicon glyphicon-star"></span>
												<span class="glyphicon glyphicon-star"></span>
												<span class="glyphicon glyphicon-star-empty"></span>
												<span class="glyphicon glyphicon-star-empty"></span>
											</span>
											<span class="reviewAuthor">
												Sherwin Rhey Condez
											</span>
											<small class="reviewTimestamp">18 April 2016</small>
										</div>								
										<div class="col-xs-12">
											<p>What a great place. I can;t say enough good things about it.</p>
										</div>
									</div>
								</div>
							</div>		
						</div>						
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-3">
			
			</div>
		</div>


