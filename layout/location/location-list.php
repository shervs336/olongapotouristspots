
	<!--- List of Olongapo Tourists Spots --->
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page-header">Olongapo Tourist Spots Lists</h1>
			</div>
			<div class="col-md-8">
				<div class="list-group">
					<div class="col-xs-12 list-group-item">
						<h4>
							<a href="<?php echo $URI; ?>location/info.php?id=1">Marikit Park</a>
							<small class="pull-right">
								<span class="glyphicon glyphicon-star"></span>
								<span class="glyphicon glyphicon-star"></span>
								<span class="glyphicon glyphicon-star"></span>
								<span class="glyphicon glyphicon-star-empty"></span>
								<span class="glyphicon glyphicon-star-empty"></span>
							</small>					
						</h4>
						<p>Hospital Rd, Olongapo, Zambales, Philippines</p>
						<p>
							<span class="label label-warning">Family</span>
							<span class="label label-warning">Park</span>
							<span class="label label-warning">Playground</span>
						</p>
					</div>
					<div class="col-xs-12 list-group-item">
						<h4>
							<a href="#">Tatlong Crus</a>
							<small class="pull-right">
								<span class="glyphicon glyphicon-star"></span>
								<span class="glyphicon glyphicon-star"></span>
								<span class="glyphicon glyphicon-star"></span>
								<span class="glyphicon glyphicon-star"></span>
								<span class="glyphicon glyphicon-star-empty"></span>
							</small>					
						</h4>
						<p>Tabacuhan, Sta. Rita, Olongapo, Zambales, Philippines</p>
						<p>
							<span class="label label-warning">Religion</span>
							<span class="label label-warning">Hiking</span>
						</p>
					</div>
					<div class="col-xs-12 list-group-item">
						<h4>
							<a href="#">Grotto</a>
							<small class="pull-right">
								<span class="glyphicon glyphicon-star"></span>
								<span class="glyphicon glyphicon-star"></span>
								<span class="glyphicon glyphicon-star"></span>
								<span class="glyphicon glyphicon-star"></span>
								<span class="glyphicon glyphicon-star"></span>
							</small>					
						</h4>
						<p>Grotto Mabayuan, Olongapo, Zambales, Philippines</p>
						<p>
							<span class="label label-warning">Religion</span>
							<span class="label label-warning">Hiking</span>
						</p>
					</div>
					<div class="col-xs-12 list-group-item">
						<h4>
							<a href="#">Parola Lighthouse</a>
							<small class="pull-right">
								<span class="glyphicon glyphicon-star"></span>
								<span class="glyphicon glyphicon-star"></span>
								<span class="glyphicon glyphicon-star"></span>
								<span class="glyphicon glyphicon-star-empty"></span>
								<span class="glyphicon glyphicon-star-empty"></span>
							</small>					
						</h4>
						<p>Kalaklan, Olongapo, Zambales, Philippines</p>
						<p>
							<span class="label label-warning">By the sea</span>
						</p>
					</div>
				</div>
				
			</div>
			<div class="col-md-4">
			</div>
		</div>


