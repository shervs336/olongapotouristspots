	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo $URI;?>">Olongapo Tourist Spots</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="<?php echo $active === "home" ? "active" : ""; ?>"><a href="<?php echo $URI;?>">Home</a></li>
					<li class="<?php echo $active === "location" ? "active" : ""; ?>"><a href="<?php echo $URI;?>location/list.php">Locations</a></li>
					<li class="<?php echo $active === "about" ? "active" : ""; ?>"><a href="<?php echo $URI;?>about.php">About</a></li>
				</ul>
				<form class="navbar-form navbar-right">
					<div class="form-group">
					  <input type="text" placeholder="Email" class="form-control">
					</div>
					<div class="form-group">
					  <input type="password" placeholder="Password" class="form-control">
					</div>
					<button type="submit" class="btn btn-success">Sign in</button>
				</form>
			</div><!--/.navbar-collapse -->
		</div>
	</nav>